## 1.循环引用

```js
在src/typings/jwt.ts
import { IUserDocument } from "../models/user";
然后
在src/models/user.ts
import { UserPayload } from '../typings/jwt';
jwt.ts引用了user.ts，user.ts引用了jwt.ts，不会造成循环引用吗
```

### 1.1 jwt.ts

```js
import user from "./user";

export default "jwt";
console.log("jwt里面打印user ", user);
```

### 1.2 user.js

```js
import jwt from "./jwt";
export default "user";
console.log("user里面打印jwt ", jwt);
```

## 2.IUserDocument

```js
export interface IUserDocument extends Document {
  username: string,
  password: string,
  email: string,
  avatar: string,
  generateToken: () => string,
  _doc: IUserDocument
}
接口中调用_doc:IUserDocument，是做什么,可以调用自己吗?


<!--../models/user 文件-->
import { UserPayload } from "../typings/jwt"

<!--../typings/jwt 文件-->
import { IUserDocument } from "../models/user";

export interface UserPayload {
    id: IUserDocument['_id']
}
两个文件相互调用,为什么不在声明了IUserDocument,在声明UserPayload思路清晰一点
```

src\controller\slider.ts

```diff
import { Request, Response } from "express";
import { ISliderDocument, Slider } from "../models";
export const list = async (_req: Request, res: Response) => {
  let sliders: ISliderDocument[] = await Slider.find();
+  console.log("_doc", sliders[0]._doc);
  res.json({ success: true, data: sliders });
};
```

src\models\slider.ts

```diff
const SliderSchema: Schema<ISliderDocument> = new Schema(
  {
    url: String,
  },
  {
    timestamps: true,
    toJSON: {
      transform: function (_doc: any, result: any) {
+        console.log("transform slider");
        result.id = result._id;
        delete result._id;
        delete result.__v;
        delete result.createdAt;
        delete result.updatedAt;
        return result;
      },
    },
  }
);
```

## 3.validate

- 这里的 payload 是 validate()应该是一个 promise
- src\store\actions\profile.tsx 里
- 为啥这里的 payload 能取值 success

src\store\reducers\profile.tsx

### 3.1 src\store\actions\profile.tsx

```js
  //https://github.com/redux-utilities/redux-promise/blob/master/src/index.js
  validate(): AnyAction {
    //发起判断当前用户是否登录的请求
    return {
      type: TYPES.VALIDATE,
      payload: validate(),
    };
  },
```

### 3.2 src\store\reducers\profile.tsx

src\store\reducers\profile.tsx

```diff
+if (action.payload.success) {
        //如果此用户已经登录了
        return {
          ...state,
          loginState: LOGIN_TYPES.LOGINED,
          user: action.payload.data, //设置用户名
          error: null, //没有错误
        };
```

https://gitee.com/zhufengpeixun/react-training-camp/issues/I1LBL3

## 4.await

- 这里为什么采用这中方式来运行呢

```js
;(async function () {
    mongoose.set('useNewUrlParser', true)
    mongoose.set('useUnifiedTopology', true)
    await mongoose.connect('mongodb://localhost/zhufengketang')
    app.listen(PORT, () => {
    console.log(Running on http://localhost:${PORT})
    })
})()
```

## 5.package.json

- 后端代码和前端代码可以共用一个 package.json 吗？每次启动的时候运行一个 start 然后同时启动前后端的项目呢。。

## 6.any

- 如果真实开发中，遇到了 不知道的 ts 类型，，又不允许写 any ，应该怎么入手解决这个类型问题。

## 7.src\api\profile.tsx

- 3 个 T
- 泛型修饰什么

### 7.1 src\store\actions\profile.tsx

src\store\actions\profile.tsx

```diff
  register(values: RegisterPayload) {
    return function (dispatch: any) {
      (async function () {
        try {
+         let result: RegisterResult = await register<RegisterResult>(values);
          if (result.success) {
            dispatch(push("/login"));
          } else {
            message.error(result.message);
          }
        } catch (error) {
          message.error("注册失败");
        }
      })();
    };
  },
```

### 7.2 src\api\profile.tsx

```js
export function register<T>(values: RegisterPayload) {
  return axios.post < T, T > ("/user/register", values);
}
```

## 8.thunk promise

- 什么时候用上面那个方法走 promise 中间件，什么时候用下面那个方法走 thunk 中间件，感觉两个方法是可以互相变通的
- 自执行函数外面为什么要套一个 return function（）{},不写行吗？

```js
  register(values: RegisterPayload) {
    return function (dispatch: any) {
      (async function () {
        try {
          let result: RegisterResult = await register<RegisterResult>(values);
          if (result.success) {
            dispatch(push("/login"));
          } else {
            message.error(result.message);
          }
        } catch (error) {
          message.error("注册失败");
        }
      })();
    };
  },
```

```js
  register(values: RegisterPayload) {
    return async function (dispatch: any) {
      //(async function () {
      try {
        let result: RegisterResult = await register<RegisterResult>(values);
        if (result.success) {
          dispatch(push("/login"));
        } else {
          message.error(result.message);
        }
      } catch (error) {
        message.error("注册失败");
      }
      //})();
    };
  },
```

## 9.store 中的这种闭包的写法是常态吗？可以采用定义一个函数然后调用的方式吗？

## 10.创建 ref 用来做什么， 不传 ref 好像也可以的

```js
function Home(props: Props) {
  const homeContainerRef = useRef(null);
  const lessonListRef = useRef(null);
  useEffect(() => {
    loadMore(homeContainerRef.current, props.getLessons);
    downRefresh(homeContainerRef.current, props.refreshLessons);
    homeContainerRef.current.addEventListener("scroll", () =>
      lessonListRef.current()
    );
    if (props.lessons) {
      homeContainerRef.current.scrollTop = store.get("homeScrollTop");
    }
    return () => {
      store.set("homeScrollTop", homeContainerRef.current.scrollTop);
    };
  }, []);
  return (
    <>
      <HomeHeader
        currentCategory={props.currentCategory}
        setCurrentCategory={props.setCurrentCategory}
        refreshLessons={props.refreshLessons}
      />
      <div className="home-container" ref={homeContainerRef}>
        <HomeSliders sliders={props.sliders} getSliders={props.getSliders} />
        <LessonList
          ref={lessonListRef}
          container={homeContainerRef}
          lessons={props.lessons}
          getLessons={props.getLessons}
        />
      </div>
    </>
  );
}
```

## 11.push

- 退出登录时 为什么 2 次 push('/login')

```js
<Button
  type={"primary"}
  onClick={async () => {
    await props.logout();
    props.history.push("/login");
  }}
>
  退出登录
</Button>
```

## 12.push

- 不是说 actions 中存放的都是普通对象吗？为什么这个项目里的 action 都是函数呢？
- 老师能不能把本项目里 redux 的用法串讲一下？
- 比如挑 client/src/store/actions/home 里的 setCurrentCategory 这个 action 讲一下

```js
export default {
  setCurrentCategory(currentCategory: string) {
    return { type: TYPES.SET_CURRENT_CATEGORY, payload: currentCategory };
  },
```

## 13.想简单了解下下拉刷新的思路

- 另外 src\routes\Home\components\LessonList\index.tsx 中的 props.lessons.list.map 这个循环不是很明白，:后面为何要返回一个空 div，就是 <div key={index} style={{ height: ${8.6666667 * rem}px }}>

## 14.generateToken

- generateToken() 主要做什么的？

- models/user 文件里的写法和用法不是很清楚？

- antd 按需引入为什么设置 libraryDirectory:是 "es"？

- 内部是如何实现按需加载的？

- axios.post<T, T>('/user/register', values); 两个 t 是干什么分别？

```js
//实例 方法
UserSchema.methods.generateToken = function (): string {
  let payload: UserPayload = { id: this._id };
  return jwt.sign(payload, process.env.JWT_SECRET_KEY!, { expiresIn: "1h" });
};
//钩子
UserSchema.pre<IUserDocument>("save", async function (next: HookNextFunction) {
  if (!this.isModified("password")) {
    return next();
  }
  try {
    this.password = await bcrypt.hash(this.password, 10);
    next();
  } catch (error) {
    next(error);
  }
});
//静态方法
UserSchema.static("login", async function (
  this: any,
  username: string,
  password: string
): Promise<IUserDocument | null> {
  let user: IUserDocument | null = await this.model("User").findOne({
    username,
  });
  if (user) {
    const matched = await bcrypt.compare(password, user.password);
    if (matched) {
      return user;
    } else {
      return null;
    }
  }
  return user;
});
```

## 15.Schema 这个概念，在数据库中的含义是什么啊？

```js
import mongoose, { Schema, Document } from "mongoose";
```

## 16.这里为什么要延迟 1 秒呢

```js
export const list = async (req: Request, res: Response) => {
  let { category } = req.query;
  let offset: any = req.query.offset;
  let limit: any = req.query.limit;
  offset = isNaN(offset) ? 0 : parseInt(offset); //偏移量
  limit = isNaN(limit) ? 5 : parseInt(limit); //每页条数
  let query: Partial<ILessonDocument> = {} as ILessonDocument;
  if (category && category != "all") query.category = category as string;
  let total = await Lesson.count(query);
  let list = await Lesson.find(query)
    .sort({ order: 1 })
    .skip(offset)
    .limit(limit);
  setTimeout(function () {
    res.json({ code: 0, data: { list, hasMore: total > offset + limit } });
  }, 1000);
};
```
