
## 11.课程管理后端接口

- 本章主要编写课程管理的后台接口

本章目录

```js
.
├── package.json
├── src
│ ├── controller
│ │ ├── lesson.ts
│ │ ├── slider.ts
│ │ └── user.ts
│ ├── exceptions
│ │ └── HttpException.ts
│ ├── index.ts
│ ├── middlewares
│ │ └── errorMiddleware.ts
│ ├── models
│ │ ├── index.ts
│ │ ├── lesson.ts
│ │ ├── slider.ts
│ │ └── user.ts
│ ├── public
│ ├── typings
│ │ ├── express.d.ts
│ │ └── jwt.ts
│ └── utils
│     └── validator.ts
└── tsconfig.json
```

本章效果
![lessonlistinterface](http://img.zhufengpeixun.cn/lessonlistinterface.gif)

### 11.1 src\index.ts

src\index.ts

```diff
import express, { Express, Request, Response, NextFunction } from "express";
import mongoose from "mongoose";
import HttpException from "./exceptions/HttpException";
import cors from "cors";
import morgan from "morgan";
import helmet from "helmet";
import errorMiddleware from "./middlewares/errorMiddleware";
import * as userController from "./controller/user";
import * as sliderController from "./controller/slider";
+import * as lessonController from "./controller/lesson";
import "dotenv/config";
import multer from "multer";
import path from "path";
+import { Slider, Lesson } from "./models";
const storage = multer.diskStorage({
  destination: path.join(__dirname, "public", "uploads"),
  filename(_req: Request, file: Express.Multer.File, cb) {
    cb(null, Date.now() + path.extname(file.originalname));
  },
});
const upload = multer({ storage });
const app: Express = express();
app.use(morgan("dev"));
app.use(cors());
app.use(helmet());
app.use(express.static(path.resolve(__dirname, "public")));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.get("/", (_req: Request, res: Response) => {
  res.json({ success: true, message: "hello world" });
});
app.get("/user/validate", userController.validate);
app.post("/user/register", userController.register);
app.post("/user/login", userController.login);
app.post(
  "/user/uploadAvatar",
  upload.single("avatar"),
  userController.uploadAvatar
);
app.get("/slider/list", sliderController.list);
+app.get("/lesson/list", lessonController.list);
+app.get("/lesson/:id", lessonController.get);
app.use((_req: Request, _res: Response, next: NextFunction) => {
  const error: HttpException = new HttpException(404, "Route not found");
  next(error);
});
app.use(errorMiddleware);
const PORT: number = (process.env.PORT && parseInt(process.env.PORT)) || 8000;
(async function () {
  mongoose.set("useNewUrlParser", true);
  mongoose.set("useUnifiedTopology", true);
  await mongoose.connect("mongodb://localhost/zhufengketang");
  await createSliders();
+  await createLessons();
  app.listen(PORT, () => {
    console.log(`Running on http://localhost:${PORT}`);
  });
})();

async function createSliders() {
  const sliders = await Slider.find();
  if (sliders.length == 0) {
    const initSliders: any = [
      { url: "http://img.zhufengpeixun.cn/post_reactnative.png" },
      { url: "http://img.zhufengpeixun.cn/post_react.png" },
      { url: "http://img.zhufengpeixun.cn/post_vue.png" },
      { url: "http://img.zhufengpeixun.cn/post_wechat.png" },
      { url: "http://img.zhufengpeixun.cn/post_architect.jpg" },
    ];
    Slider.create(initSliders);
  }
}

+async function createLessons() {
+  const lessons = await Lesson.find();
+  if (lessons.length == 0) {
+    const lessons: any = [
+      {
+        order: 1,
+        title: "1.React全栈架构",
+        video: "http://img.zhufengpeixun.cn/gee2.mp4",
+        poster: "http://img.zhufengpeixun.cn/react_poster.jpg",
+        url: "http://img.zhufengpeixun.cn/react_url.png",
+        price: "¥100.00元",
+        category: "react",
+      },
+      {
+        order: 2,
+        title: "2.React全栈架构",
+        video: "http://img.zhufengpeixun.cn/gee2.mp4",
+        poster: "http://img.zhufengpeixun.cn/react_poster.jpg",
+        url: "http://img.zhufengpeixun.cn/react_url.png",
+        price: "¥200.00元",
+        category: "react",
+      },
+      {
+        order: 3,
+        title: "3.React全栈架构",
+        video: "http://img.zhufengpeixun.cn/gee2.mp4",
+        poster: "http://img.zhufengpeixun.cn/react_poster.jpg",
+        url: "http://img.zhufengpeixun.cn/react_url.png",
+        price: "¥300.00元",
+        category: "react",
+      },
+      {
+        order: 4,
+        title: "4.React全栈架构",
+        video: "http://img.zhufengpeixun.cn/gee2.mp4",
+        poster: "http://img.zhufengpeixun.cn/react_poster.jpg",
+        url: "http://img.zhufengpeixun.cn/react_url.png",
+        price: "¥400.00元",
+        category: "react",
+      },
+      {
+        order: 5,
+        title: "5.React全栈架构",
+        video: "http://img.zhufengpeixun.cn/gee2.mp4",
+        poster: "http://img.zhufengpeixun.cn/react_poster.jpg",
+        url: "http://img.zhufengpeixun.cn/react_url.png",
+        price: "¥500.00元",
+        category: "react",
+      },
+      {
+        order: 6,
+        title: "6.Vue从入门到项目实战",
+        video: "http://img.zhufengpeixun.cn/gee2.mp4",
+        poster: "http://img.zhufengpeixun.cn/vue_poster.png",
+        url: "http://img.zhufengpeixun.cn/vue_url.png",
+        price: "¥100.00元",
+        category: "vue",
+      },
+      {
+        order: 7,
+        title: "7.Vue从入门到项目实战",
+        video: "http://img.zhufengpeixun.cn/gee2.mp4",
+        poster: "http://img.zhufengpeixun.cn/vue_poster.png",
+        url: "http://img.zhufengpeixun.cn/vue_url.png",
+        price: "¥200.00元",
+        category: "vue",
+      },
+      {
+        order: 8,
+        title: "8.Vue从入门到项目实战",
+        video: "http://img.zhufengpeixun.cn/gee2.mp4",
+        poster: "http://img.zhufengpeixun.cn/vue_poster.png",
+        url: "http://img.zhufengpeixun.cn/vue_url.png",
+        price: "¥300.00元",
+        category: "vue",
+      },
+      {
+        order: 9,
+        title: "9.Vue从入门到项目实战",
+        video: "http://img.zhufengpeixun.cn/gee2.mp4",
+        poster: "http://img.zhufengpeixun.cn/vue_poster.png",
+        url: "http://img.zhufengpeixun.cn/vue_url.png",
+        price: "¥400.00元",
+        category: "vue",
+      },
+      {
+        order: 10,
+        title: "10.Vue从入门到项目实战",
+        video: "http://img.zhufengpeixun.cn/gee2.mp4",
+        poster: "http://img.zhufengpeixun.cn/vue_poster.png",
+        url: "http://img.zhufengpeixun.cn/vue_url.png",
+        price: "¥500.00元",
+        category: "vue",
+      },
+      {
+        order: 11,
+        title: "11.React全栈架构",
+        video: "http://img.zhufengpeixun.cn/gee2.mp4",
+        poster: "http://img.zhufengpeixun.cn/react_poster.jpg",
+        url: "http://img.zhufengpeixun.cn/react_url.png",
+        price: "¥600.00元",
+        category: "react",
+      },
+      {
+        order: 12,
+        title: "12.React全栈架构",
+        video: "http://img.zhufengpeixun.cn/gee2.mp4",
+        poster: "http://img.zhufengpeixun.cn/react_poster.jpg",
+        url: "http://img.zhufengpeixun.cn/react_url.png",
+        price: "¥700.00元",
+        category: "react",
+      },
+      {
+        order: 13,
+        title: "13.React全栈架构",
+        video: "http://img.zhufengpeixun.cn/gee2.mp4",
+        poster: "http://img.zhufengpeixun.cn/react_poster.jpg",
+        url: "http://img.zhufengpeixun.cn/react_url.png",
+        price: "¥800.00元",
+        category: "react",
+      },
+      {
+        order: 14,
+        title: "14.React全栈架构",
+        video: "http://img.zhufengpeixun.cn/gee2.mp4",
+        poster: "http://img.zhufengpeixun.cn/react_poster.jpg",
+        url: "http://img.zhufengpeixun.cn/react_url.png",
+        price: "¥900.00元",
+        category: "react",
+      },
+      {
+        order: 15,
+        title: "15.React全栈架构",
+        video: "http://img.zhufengpeixun.cn/gee2.mp4",
+        poster: "http://img.zhufengpeixun.cn/react_poster.jpg",
+        url: "http://img.zhufengpeixun.cn/react_url.png",
+        price: "¥1000.00元",
+        category: "react",
+      },
+      {
+        order: 16,
+        title: "16.Vue从入门到项目实战",
+        video: "http://img.zhufengpeixun.cn/gee2.mp4",
+        poster: "http://img.zhufengpeixun.cn/vue_poster.png",
+        url: "http://img.zhufengpeixun.cn/vue_url.png",
+        price: "¥600.00元",
+        category: "vue",
+      },
+      {
+        order: 17,
+        title: "17.Vue从入门到项目实战",
+        video: "http://img.zhufengpeixun.cn/gee2.mp4",
+        poster: "http://img.zhufengpeixun.cn/vue_poster.png",
+        url: "http://img.zhufengpeixun.cn/vue_url.png",
+        price: "¥700.00元",
+        category: "vue",
+      },
+      {
+        order: 18,
+        title: "18.Vue从入门到项目实战",
+        video: "http://img.zhufengpeixun.cn/gee2.mp4",
+        poster: "http://img.zhufengpeixun.cn/vue_poster.png",
+        url: "http://img.zhufengpeixun.cn/vue_url.png",
+        price: "¥800.00元",
+        category: "vue",
+      },
+      {
+        order: 19,
+        title: "19.Vue从入门到项目实战",
+        video: "http://img.zhufengpeixun.cn/gee2.mp4",
+        poster: "http://img.zhufengpeixun.cn/vue_poster.png",
+        url: "http://img.zhufengpeixun.cn/vue_url.png",
+        price: "¥900.00元",
+        category: "vue",
+      },
+      {
+        order: 20,
+        title: "20.Vue从入门到项目实战",
+        video: "http://img.zhufengpeixun.cn/gee2.mp4",
+        poster: "http://img.zhufengpeixun.cn/vue_poster.png",
+        url: "http://img.zhufengpeixun.cn/vue_url.png",
+        price: "¥1000.00元",
+        category: "vue",
+      },
+    ];
+    Lesson.create(lessons);
  }
}
```

### 11.2 src\models\index.ts

src\models\index.ts

```diff
export * from './user';
export * from './slider';
+export * from './lesson';
```

### 11.3 controller\lesson.ts

src\controller\lesson.ts

```js
import { Request, Response } from "express";
import { ILessonDocument, Lesson } from "../models";
export const list = async (req: Request, res: Response) => {
  let { category } = req.query;
  let offset: any = req.query.offset;
  let limit: any = req.query.limit;
  offset = isNaN(offset) ? 0 : parseInt(offset); //偏移量
  limit = isNaN(limit) ? 5 : parseInt(limit); //每页条数
  let query: Partial<ILessonDocument> = {} as ILessonDocument;
  if (category && category != "all") query.category = category as string;
  let total = await Lesson.count(query);
  let list = await Lesson.find(query)
    .sort({ order: 1 })
    .skip(offset)
    .limit(limit);
  setTimeout(function () {
    res.json({ code: 0, data: { list, hasMore: total > offset + limit } });
  }, 1000);
};
export const get = async (req: Request, res: Response) => {
  let id = req.params.id;
  let lesson = await Lesson.findById(id);
  res.json({ success: true, data: lesson });
};

```

### 11.4 models\lesson.ts

src\models\lesson.ts

```js
import mongoose, { Schema, Document } from "mongoose";
export interface ILessonDocument extends Document {
  order: number; //顺序
  title: string; //标题
  video: string; //视频
  poster: string; //海报
  url: string; //url地址
  price: string; //价格
  category: string; //分类
  _doc: ILessonDocument;
}
const LessonSchema: Schema<ILessonDocument> = new Schema(
  {
    order: Number, //顺序
    title: String, //标题
    video: String, //视频
    poster: String, //海报
    url: String, //url地址
    price: String, //价格
    category: String, //分类
  },
  { timestamps: true }
);

export const Lesson =
  mongoose.model < ILessonDocument > ("Lesson", LessonSchema);
```

## 12.首页的课程列表

- 本章实践的内容
  - 加载课程列表
  - 上拉加载
  - 下拉刷新
  - 防抖
  - 节流
  - 虚拟列表优化

课程目录

```js
├── package.json
├── src
│   ├── api
│   │   ├── home.tsx
│   │   ├── index.tsx
│   │   └── profile.tsx
│   ├── assets
│   │   ├── css
│   │   │   └── common.less
│   │   └── images
│   │       └── logo.png
│   ├── components
│   │   ├── LessonList
│   │   │   ├── index.less
│   │   │   └── index.tsx
│   │   ├── NavHeader
│   │   │   ├── index.less
│   │   │   └── index.tsx
│   │   └── Tabs
│   │       ├── index.less
│   │       └── index.tsx
│   ├── index.html
│   ├── index.tsx
│   ├── routes
│   │   ├── Home
│   │   │   ├── components
│   │   │   │   ├── HomeHeader
│   │   │   │   │   ├── index.less
│   │   │   │   │   └── index.tsx
│   │   │   │   ├── HomeSliders
│   │   │   │   │   ├── index.less
│   │   │   │   │   └── index.tsx
│   │   │   │   └── LessonList
│   │   │   │       ├── index.less
│   │   │   │       └── index.tsx
│   │   │   ├── index.less
│   │   │   └── index.tsx
│   │   ├── Login
│   │   │   ├── index.less
│   │   │   └── index.tsx
│   │   ├── Mine
│   │   │   └── index.tsx
│   │   ├── Profile
│   │   │   ├── index.less
│   │   │   └── index.tsx
│   │   └── Register
│   │       ├── index.less
│   │       └── index.tsx
│   ├── store
│   │   ├── actions
│   │   │   ├── home.tsx
│   │   │   └── profile.tsx
│   │   ├── action-types.tsx
│   │   ├── history.tsx
│   │   ├── index.tsx
│   │   └── reducers
│   │       ├── home.tsx
│   │       ├── index.tsx
│   │       ├── mime.tsx
│   │       └── profile.tsx
│   ├── typings
│   │   ├── images.d.ts
│   │   ├── lesson.tsx
│   │   ├── login-types.tsx
│   │   ├── slider.tsx
│   │   └── user.tsx
│   └── utils.tsx
├── tsconfig.json
└── webpack.config.js
```

页面效果
![lessonlist2](http://img.zhufengpeixun.cn/lessonlist2.gif)

### 12.1 src\api\home.tsx

src\api\home.tsx

```diff
import axios from "./index";
export function getSliders() {
  return axios.get("/slider/list");
}
+export function getLessons(
+  currentCategory: string = "all",
+  offset: number,
+  limit: number
+) {
+  return axios.get(
+    `/lesson/list?category=${currentCategory}&offset=${offset}&limit=${limit}`
+  );
+}

```

### 12.2 action-types.tsx

src\store\action-types.tsx

```diff
export const ADD = "ADD";
//设置当前分类的名称
export const SET_CURRENT_CATEGORY = "SET_CURRENT_CATEGORY";
//发起验证用户是否登录的请求
export const VALIDATE = "VALIDATE";

export const LOGOUT = "LOGOUT";
//上传头像
export const CHANGE_AVATAR = "CHANGE_AVATAR";
export const GET_SLIDERS = "GET_SLIDERS";

+export const GET_LESSONS = "GET_LESSONS";
+export const SET_LESSONS_LOADING = "SET_LESSONS_LOADING";
+export const SET_LESSONS = "SET_LESSONS";
+export const REFRESH_LESSONS = "REFRESH_LESSONS";
```

### 12.3 typings\lesson.tsx

src\typings\lesson.tsx

```js
export default interface Lesson {
  id: string;
  title: string;
  video: string;
  poster: string;
  url: string;
  price: string;
  category: string;
}

export interface LessonResult {
  data: Lesson;
  success: boolean;
}
```

### 12.4 reducers\home.tsx

src\store\reducers\home.tsx

```diff
import { AnyAction } from "redux";
import * as TYPES from "../action-types";
import Slider from "@/typings/slider";
+import Lesson from "@/typings/Lesson";

+export interface Lessons {
+  loading: boolean;
+  list: Lesson[];
+  hasMore: boolean;
+  offset: number;
+  limit: number;
+}
export interface HomeState {
  currentCategory: string;
  sliders: Slider[];
+  lessons: Lessons;
}
let initialState: HomeState = {
  currentCategory: "all", //默认当前的分类是显示全部类型的课程
  sliders: [],
+  lessons: {
+    loading: false,
+    list: [],
+    hasMore: true,
+    offset: 0,
+    limit: 5,
+  },
};
export default function (
  state: HomeState = initialState,
  action: AnyAction
): HomeState {
  switch (action.type) {
    case TYPES.SET_CURRENT_CATEGORY: //修改当前分类
      return { ...state, currentCategory: action.payload };
    case TYPES.GET_SLIDERS:
      return { ...state, sliders: action.payload.data };
+    case TYPES.SET_LESSONS_LOADING:
+      return {
+        ...state,
+        lessons: { ...state.lessons, loading: action.payload },
+      };
+    case TYPES.SET_LESSONS:
+      return {
+        ...state,
+        lessons: {
+          ...state.lessons,
+          loading: false,
+          hasMore: action.payload.hasMore,
+          list: [...state.lessons.list, ...action.payload.list],
+          offset: state.lessons.offset + action.payload.list.length,
+        },
+      };
+    case TYPES.REFRESH_LESSONS:
+      return {
+        ...state,
+        lessons: {
+          ...state.lessons,
+          loading: false,
+          hasMore: action.payload.hasMore,
+          list: action.payload.list,
+          offset: action.payload.list.length,
+        },
+      };
    default:
      return state;
  }
}

```

### 12.5 actions\home.tsx

src\store\actions\home.tsx

```diff
import * as TYPES from "../action-types";
+import { getSliders, getLessons } from "@/api/home";
export default {
  setCurrentCategory(currentCategory: string) {
    return { type: TYPES.SET_CURRENT_CATEGORY, payload: currentCategory };
  },
  getSliders() {
    return {
      type: TYPES.GET_SLIDERS,
      payload: getSliders(),
    };
  },
+  getLessons() {
+    return (dispatch: any, getState: any) => {
+      (async function () {
+        let {
+          currentCategory,
+          lessons: { hasMore, offset, limit, loading },
+        } = getState().home;
+        if (hasMore && !loading) {
+          dispatch({ type: TYPES.SET_LESSONS_LOADING, payload: true });
+          let result = await getLessons(currentCategory, offset, limit);
+          dispatch({ type: TYPES.SET_LESSONS, payload: result.data });
+        }
+      })();
+    };
+  },
+  refreshLessons() {
+    return (dispatch: any, getState: any) => {
+      (async function () {
+        let {
+          currentCategory,
+          lessons: { limit, loading },
+        } = getState().home;
+        if (!loading) {
+          dispatch({ type: TYPES.SET_LESSONS_LOADING, payload: true });
+          let result = await getLessons(currentCategory, 0, limit);
+          dispatch({ type: TYPES.REFRESH_LESSONS, payload: result.data });
+        }
+      })();
+    };
+  },
};

```

### 12.6 src\utils.tsx

src\utils.tsx

```js
//ele 要实现此功能DOM对象 callback加载更多的方法
export function loadMore(element: any, callback: any) {
  function _loadMore() {
    let clientHeight = element.clientHeight;
    let scrollTop = element.scrollTop;
    let scrollHeight = element.scrollHeight;
    if (clientHeight + scrollTop + 10 >= scrollHeight) {
      callback();
    }
  }
  element.addEventListener("scroll", debounce(_loadMore, 300));
}
export function downRefresh(element: HTMLDivElement, callback: Function) {
  let startY: number; //变量，存储接下时候的纵坐标
  let distance: number; //本次下拉的距离
  let originalTop = element.offsetTop; //最初此元素距离顶部的距离 top=50
  let startTop: number;
  let $timer: any = null;
  element.addEventListener("touchstart", function (event: TouchEvent) {
    if ($timer) clearInterval($timer);
    let touchMove = throttle(_touchMove, 30);
    //只有当此元素处于原始位置才能下拉，如果处于回弹的过程则不能拉了.并且此元素向上卷去的高度==0
    if (element.scrollTop === 0) {
      startTop = element.offsetTop;
      startY = event.touches[0].pageY; //记录当前点击的纵坐标
      element.addEventListener("touchmove", touchMove);
      element.addEventListener("touchend", touchEnd);
    }

    function _touchMove(event: TouchEvent) {
      let pageY = event.touches[0].pageY; //拿到最新的纵坐标
      if (pageY > startY) {
        distance = pageY - startY;
        element.style.top = startTop + distance + "px";
      } else {
        element.removeEventListener("touchmove", touchMove);
        element.removeEventListener("touchend", touchEnd);
      }
    }

    function touchEnd(_event: TouchEvent) {
      element.removeEventListener("touchmove", touchMove);
      element.removeEventListener("touchend", touchEnd);
      if (distance > 30) {
        callback();
      }
      $timer = setInterval(() => {
        let currentTop = element.offsetTop;
        if (currentTop - originalTop > 1) {
          element.style.top = currentTop - 1 + "px";
        } else {
          element.style.top = originalTop + "px";
        }
      }, 13);
    }
  });
}

export function debounce(fn: any, wait: number) {
  var timeout: any = null;
  return function () {
    if (timeout !== null) clearTimeout(timeout);
    timeout = setTimeout(fn, wait);
  };
}
export function throttle(func: any, delay: number) {
  var prev = Date.now();
  return function () {
    var context = this;
    var args = arguments;
    var now = Date.now();
    if (now - prev >= delay) {
      func.apply(context, args);
      prev = Date.now();
    }
  };
}

export const store = {
  set(key: string, val: string) {
    sessionStorage.setItem(key, val);
  },
  get(key: string) {
    return sessionStorage.getItem(key);
  },
};
```

### 12.7 LessonList\index.tsx

src\routes\Home\components\LessonList\index.tsx

```js
import React, { useEffect, forwardRef, useState } from "react";
import "./index.less";
import { Card, Skeleton, Button, Alert, Menu } from "antd";
import { Link } from "react-router-dom";
import Lesson from "@/typings/lesson";
import { MenuOutlined } from "@ant-design/icons";
interface Props {
  children?: any;
  lessons?: any;
  getLessons?: any;
  container?: any;
}

function LessonList(props: Props, lessonListRef: any) {
  const [_, forceUpdate] = useState(0);
  useEffect(() => {
    if (props.lessons.list.length == 0) {
      props.getLessons();
    }
    lessonListRef.current = () => forceUpdate((x) => x + 1);
  }, []);

  let start = 0;
  let rem = parseInt(document.documentElement.style.fontSize); //1个rem的像素
  console.log("rem", rem); //37px
  if (props.container.current) {
    let scrollTop = props.container.current.scrollTop; //获得向上卷去的高度
    //7 * rem是课程列表上面的内容的高度
    if (scrollTop - 7 * rem > 0) {
      //卷去的高度减去 7 * rem 才是真正课程列表向上卷去的高度 一个卡片是8.67 * rem
      start = Math.floor((scrollTop - 7 * rem) / (8.67 * rem));
    }
    console.log("rem", rem);
    console.log("start", start);
  }
  return (
    <section className="lesson-list">
      <h2>
        <MenuOutlined /> 全部课程
      </h2>
      <Skeleton
        loading={props.lessons.list.length == 0 && props.lessons.loading}
        active
        paragraph={{ rows: 8 }}
      >
        {props.lessons.list.map((lesson: Lesson, index: number) =>
          (start < 5 && index <= 5) || (index >= start && index < start + 5) ? (
            <Link
              key={lesson.id}
              to={{ pathname: `/detail/${lesson.id}`, state: lesson }}
            >
              <Card
                hoverable={true}
                style={{ width: "100%" }}
                cover={<img alt={lesson.title} src={lesson.poster} />}
              >
                <Card.Meta
                  title={lesson.title}
                  description={`价格: ¥${lesson.price}元`}
                />
              </Card>
            </Link>
          ) : (
            <div key={index} style={{ height: `${8.6666667 * rem}px` }}></div>
          )
        )}
        {props.lessons.hasMore ? (
          <Button
            onClick={props.getLessons}
            loading={props.lessons.loading}
            type="primary"
            block
          >
            {props.lessons.loading ? "" : "加载更多"}
          </Button>
        ) : (
          <Alert
            style={{ textAlign: "center" }}
            message="到底了"
            type="warning"
          />
        )}
      </Skeleton>
    </section>
  );
}
export default forwardRef(LessonList);
```

src\routes\Home\components\LessonList\index.less

```less
.lesson-list {
  h2 {
    line-height: 100px;
    i {
      margin: 0 10px;
    }
  }
  .ant-card.ant-card-bordered.ant-card-hoverable {
    height: 650px;
    overflow: hidden;
  }
}
```

### 12.8 routes\Home\index.tsx

src\routes\Home\index.tsx

```diff
+import React, { PropsWithChildren, useRef, useEffect } from "react";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router-dom";
import actions from "@/store/actions/home";
import HomeHeader from "./components/HomeHeader";
import { CombinedState } from "@/store/reducers";
import { HomeState } from "@/store/reducers/home";
import HomeSliders from "./components/HomeSliders";
import "./index.less";
+import LessonList from "./components/LessonList";
+import { loadMore, downRefresh, store, debounce, throttle } from "@/utils";
type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof actions;
interface Params {}
type Props = PropsWithChildren<
  RouteComponentProps<Params> & StateProps & DispatchProps
>;
function Home(props: Props) {
  const homeContainerRef = useRef(null);
+  const lessonListRef = useRef(null);
+  useEffect(() => {
+    loadMore(homeContainerRef.current, props.getLessons);
+    downRefresh(homeContainerRef.current, props.refreshLessons);
+    homeContainerRef.current.addEventListener("scroll", () =>
+      lessonListRef.current()
+    );
+    if (props.lessons) {
+      homeContainerRef.current.scrollTop = store.get("homeScrollTop");
+    }
+    return () => {
+      store.set("homeScrollTop", homeContainerRef.current.scrollTop);
+    };
  }, []);
  return (
    <>
      <HomeHeader
        currentCategory={props.currentCategory}
        setCurrentCategory={props.setCurrentCategory}
        refreshLessons={props.refreshLessons}
      />
      <div className="home-container" ref={homeContainerRef}>
        <HomeSliders sliders={props.sliders} getSliders={props.getSliders} />
+        <LessonList
+          ref={lessonListRef}
+          container={homeContainerRef}
+          lessons={props.lessons}
+          getLessons={props.getLessons}
+        />
      </div>
    </>
  );
}
let mapStateToProps = (state: CombinedState): HomeState => state.home;
export default connect(mapStateToProps, actions)(Home);

```

### 12.9 routes\Home\index.less

src\routes\Home\index.less

```diff
+.home-container {
+  position: fixed;
+  top: 100px;
+  left: 0;
+  width: 100%;
+  overflow-y: auto;
+  height: calc(100vh - 222px);
+}

```